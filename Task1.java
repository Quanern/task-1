import java.util.*;

public class Task1 {
    static int column;
    static int row;
    static char symbol;

    static void drawBox() { //Draws a box based on the given size of column and row
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (i == 0 || i == row - 1)
                    System.out.print(symbol);
                else if (j == 0 || j == column - 1)
                    System.out.print(symbol);
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }

    static void drawNestedBox() { //Draws a box within the box with a space between the boxes.
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (i == 0 || i == row - 1)
                    System.out.print(symbol);
                else if (i == 2 && j >= 2 && j <= column - 3 || i == row - 3 && j >= 2 && j <= column - 3)
                    System.out.print(symbol);
                else if (j == 0 || j == column - 1)
                    System.out.print(symbol);
                else if (j == 2 && i >= 2 && i <= row - 3 || j == column - 3 && i >= 2 && i <= row - 3)
                    System.out.print(symbol);
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(
                "Welcome to task 1. You will be given a prompt to enter 2 integers and a character for the square to be printed as");
        System.out.println("Please enter how many columns the rectangle has");

        while (scanner.hasNext()) { //Open scanner, won't stop untill you exit out of the program at a later prompt.
            if (!scanner.hasNextInt()) { //If given input is not an integer
                while (!scanner.hasNextInt()) { //A while loop that tells you to input an integer.
                    System.out.println("Your input was not an integer, try again");
                    scanner.next(); //Skips the current line so it won't be in an endless loop if input is not an integer
                }
                column = scanner.nextInt(); //Size of the columns (width) will be set to the input
            }
            else
                column = scanner.nextInt();

            System.out.println("Please enter how many rows the rectangle has");
            if (!scanner.hasNextInt()) {
                while (!scanner.hasNextInt()) {
                    System.out.println("Your input was not an integer, try again");
                    scanner.next();
                }
                row = scanner.nextInt();
            }
            else
                row = scanner.nextInt();

            System.out.println("Please enter the character you want the rectangle to be made of");
            symbol = scanner.next().charAt(0); //The rectangle will be made up of the first char that is entered, even if multiple is entered.
            if (row >4 && column > 4){ //If the rectangle is big enough, the nested box will be drawn instead of the single box.
                drawNestedBox();
            }
            else if ((row >= 2 && column >= 1) || (row >= 1 && column >= 2)){
                drawBox();
            }
            else { //Can't draw the box if the given numbers are too small (1,1), which is a point or are negative.
                System.out.println("Numbers given are too small to make a rectangle");
            }
            System.out.println("If you want to exit the program, enter q, otherwise enter something else");
            String s1 = scanner.next();
            if (s1.equalsIgnoreCase("q")) {
                System.out.println("Thank you for using this program. Have a nice day");
                System.exit(0);
            }
            System.out.println("Please enter how many columns the rectangle has");
        }
        scanner.close();
    }
}