# Assignment 1
This is the first assignment for the upskill Java course at Noroff.
In this application you will be prompted to enter 3 inputs:
- Width of the rectangle.
- Height of the rectangle.
- What character the application will print the rectangle with.

Given those parameters, the application will print the rectangle.
As long as the user doesn't enter *q* after the print, but enter anything else, the application will ask for 3 new inputs.
If the given rectangle is big enough, a smaller rectangle will be drawn inside of it.
